## GitOps-style Continuous Delivery For Kubernetes Engine 

- implements: https://cloud.google.com/kubernetes-engine/docs/tutorials/gitops-cloud-build

- docker image build , push and deploy  

- configuration is in .gitlab-ci.yml file



## Docker client ( runner setup )


    - Setup a Gitlab Shell runner https://docs.gitlab.com/runner/executors/shell.html
    - Copy the kubeconfig to gtlab-runner user on the shell runner
    - Make sure you can run kubectl commands using the gitab-runner user on the runner/build machine
    - Make sure docker is installed on runner
    - Clone this project and assign/bind your specific runner to it


## Other tools

- https://composerize.com/

- http://kompose.io/

