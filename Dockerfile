FROM nginx
ENV AUTHOR=Docker

COPY default.conf /etc/nginx/conf.d/
COPY nginx.conf /etc/nginx/

WORKDIR /usr/share/nginx/html
COPY Hello_docker.html /usr/share/nginx/html

CMD cd /usr/share/nginx/html && sed -e s/Docker/"$AUTHOR"/ Hello_docker.html > index.html ; nginx -g 'daemon off;'
